#include "enterexitanimation.h"
#include <QPropertyAnimation>
#include <QProcess>
Enterexitanimation::Enterexitanimation(QWidget *parent) : QWidget(parent)
{
    closeBt = new QPushButton(this);
    setMaximumSize(QSize(40,40));
    closeBt->setGeometry(0,0,30,40);
    closeBt->setStyleSheet("QPushButton{border-image:url(:/icon/Resource/icon/menu);outline:none}"
                           "QPushButton:pressed{border-image:url(:/icon/Resource/icon/menu_clicked);}");
    connect(closeBt,SIGNAL(clicked()),this,SLOT(exit_annimation()));
}
void Enterexitanimation::setAnimationObject(QMainWindow *obj)
{
    animationObject = obj;
    /* 进场动画 */
    QPropertyAnimation *new_Animation = new QPropertyAnimation(animationObject, "geometry");
    new_Animation->setDuration(10);//动画长度是10ms
    new_Animation->setEndValue(QRect(0, 0,animationObject->geometry().width(),animationObject->geometry().height()));
    new_Animation->setStartValue(QRect(0, animationObject->geometry().height(),0, 0));
    new_Animation->start();
    connect(new_Animation,SIGNAL(finished()),this,SLOT(slot_update()));

}

void Enterexitanimation::exit_annimation()
{
    /* 退场动画 */
    QPropertyAnimation *new_Animation = new QPropertyAnimation(animationObject, "geometry");
    new_Animation->setDuration(10);//动画长度是10ms
    new_Animation->setStartValue(QRect(animationObject->geometry().x(), animationObject->geometry().y(),
                                        animationObject->geometry().width(), animationObject->geometry().height())
                                  );
    new_Animation->setEndValue(QRect(animationObject->geometry().width(),-animationObject->geometry().y(),
                                      animationObject->geometry().width(),animationObject->geometry().y()));
    new_Animation->start();
    connect(new_Animation,SIGNAL(finished()),this,SLOT(slot_close()));
}
void Enterexitanimation::slot_close()
{
    animationObject->close();
}
void Enterexitanimation::slot_update()
{
    animationObject->repaint();
}
