# qt_genshin_时钟

项目目的：
1. 了解Ubuntu linux下qt开发流程
2. 了解纯代码编写qt界面程序的开发流程
3. 掌握定时器的使用方法

项目原理
1. 用代码控制qt控件的操作
2. 用QTime获取系统时间
3. 用QTimer定时更新界面

项目运行效果如下：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0129/185252_ed1df63b_10274022.png "11.PNG")
