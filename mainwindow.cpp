#include "mainwindow.h"
#include <QtMath>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    /* 获取屏幕分辨率信息，设置主窗口的大小 */
    this->showFullScreen();
    this->setGeometry(0,0,QApplication::desktop()->width(),QApplication::desktop()->height());
    this->setWindowFlags(Qt::FramelessWindowHint);          // 主窗口无边框
    this->setStyleSheet("QWidget{ border-image: url(:/icon/Resource/image/qclockbg.png);}");

    //this->setGeometry(0,0,480,272);

    QFont font;

    font.setPixelSize((float)18/800*this->geometry().width());

    /* Label初始化 */
    timeDisplay = new QLabel(this);
    dateDisplay = new QLabel(this);

    timeDisplay->setStyleSheet("color: rgb(255,248,220);font-family: '楷体';border-image:transparent");//背景设为透明
    dateDisplay->setStyleSheet("color: rgb(255,248,220);font-family: '楷体';font: bold;border-image:transparent");

    font.setPixelSize((float)20/800*this->geometry().width());
    dateDisplay->setFont(font);
    font.setPixelSize((float)70/800*this->geometry().width());
    timeDisplay->setFont(font);

    /* 设置控件的位置 */
    timeDisplay->setGeometry((float)400/800*this->geometry().width(), (float)240/480*this->geometry().height(),
                             (float)350/800*this->geometry().width(), (float)120/480*this->geometry().height());
    dateDisplay->setGeometry((float)400/800*this->geometry().width(), (float)360/480*this->geometry().height(),
                             (float)350/800*this->geometry().width(), (float)120/480*this->geometry().height());
    timeDisplay->setAlignment(Qt::AlignRight | Qt::AlignBottom);  // 设置字体对其方式
    dateDisplay->setAlignment(Qt::AlignRight | Qt::AlignTop);
    timeDisplay->setText(QTime::currentTime().toString());        // 初始化Label显示
    dateDisplay->setText(QDate::currentDate().toString());

    /* 画笔参数初始化 */

    painPara.painterColor = QColor("#CD0000");
    painPara.translateX   = (float)200/800*this->geometry().width();
    painPara.translateY   = (float)240/480*this->geometry().height();
    painPara.scale        = (qMin((float)200/800*this->geometry().width(),
                                  (float)240/480*this->geometry().height())) / 100;

    painParah.painterColor = QColor("#1C1C1C");
    painParah.translateX   = (float)200/800*this->geometry().width();
    painParah.translateY   = (float)240/480*this->geometry().height();
    painParah.scale        = (qMin((float)200/800*this->geometry().width(),
                                  (float)240/480*this->geometry().height())) / 100;

    painParas.painterColor = QColor("#FF8C00");
    painParas.translateX   = (float)200/800*this->geometry().width();
    painParas.translateY   = (float)240/480*this->geometry().height();
    painParas.scale        = (qMin((float)200/800*this->geometry().width(),
                                  (float)240/480*this->geometry().height())) / 100;

    /* 定时器初始化 */
    timer = new QTimer(this);
    timer->start(1000);           // 开始计时，1秒一次

    /* 信号、槽连接 */
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));

    enterexitanimation = new Enterexitanimation(this);
    enterexitanimation->setGeometry(this->geometry().width()-80,0,80,80); //设置位置
    enterexitanimation->setAnimationObject(this);//设置动画对象
}

MainWindow::~MainWindow()
{

}

void MainWindow::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event);

    QPainter painter(this);
    painter.translate(painPara.translateX, painPara.translateY);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.scale(painPara.scale, painPara.scale);
    painter.setPen(painPara.painterColor);
    painter.setBrush(painPara.painterColor);

    QPainter painterh(this);
    painterh.translate(painParah.translateX, painParah.translateY);
    painterh.setRenderHint(QPainter::Antialiasing);
    painterh.scale(painParah.scale, painParah.scale);
    painterh.setPen(painParah.painterColor);
    painterh.setBrush(painParah.painterColor);


    QPainter painters(this);
    painters.translate(painParas.translateX, painParas.translateY);
    painters.setRenderHint(QPainter::Antialiasing);
    painters.scale(painParas.scale, painParas.scale);
    painters.setPen(painParas.painterColor);
    painters.setBrush(painParas.painterColor);

    /* 获取系统时间 */
    QTime time = QTime::currentTime();

    /* 时针 */
    painParah.coordinateHourHand[0] = QPoint(4,1);
    painParah.coordinateHourHand[1] = QPoint(-4,1);
    painParah.coordinateHourHand[2] = QPoint(0,-30);

    painterh.save();
    painterh.rotate(30.0 * (time.hour() + time.minute() / 60.0)); //角度转换30度
    painterh.drawConvexPolygon(painParah.coordinateHourHand, 3);   //描画的形状
    painterh.restore();

    /* 描画时针刻度线 */
    painters.save();
    painters.setPen(QColor(0,255,127));
    /* 描画时钟数字文本 */
    QFont font;
    font.setFamily("Cambria");
    font.setPointSize(8);
    painters.setFont(font);

    int nHour = 0;
    for (int i = 0; i < 12; ++i) {
        nHour = i + 3;
        if (nHour > 12)
            nHour -= 12;
        painters.drawText(textRectF(65, 10, i * 30), Qt::AlignCenter, QString::number(nHour));
    }

    for(int i = 0; i < 12; i++)
    {
        painters.rotate(30.0);
        //painterh.drawLine(80,0,90,0);
        painters.drawRect(80,-1,5,2);
    }
    painters.restore();

    /* 分针 */
    painParah.coordinateMinuteHand[0] = QPoint(3,1);
    painParah.coordinateMinuteHand[1] = QPoint(-3,1);
    painParah.coordinateMinuteHand[2] = QPoint(0,-70);
    painterh.save();
    painterh.rotate(6.0 * (time.minute() + time.second() / 60.0));
    painterh.drawConvexPolygon(painParah.coordinateMinuteHand, 3);
    painterh.restore();

    /* 描画分针刻度线 */
    painters.save();
    for(int i = 0; i<60;i++) {
        if(i % 5)
            painters.drawPoint(82,0);
        painters.rotate(6.0);
    }
    painters.restore();

    /* 秒针 */
    painPara.coordinateSecondHand[0] = QPoint(1,20);
    painPara.coordinateSecondHand[1] = QPoint(-1,20);
    painPara.coordinateSecondHand[2] = QPoint(0,-80);

    painter.save();
    painter.rotate(6.0 * time.second());
    painter.drawConvexPolygon(painPara.coordinateSecondHand, 3);
    painter.restore();

    /* 更新时间、日期显示 */
    timeDisplay->setText(time.toString());
    dateDisplay->setText(QDate::currentDate().toString());
}

/* 参考一去二三里的方法 */
QRectF MainWindow::textRectF(double radius, int pointSize, double angle)
{
    QRectF rectF;
    rectF.setX(radius*qCos(angle*M_PI/180.0) - pointSize*2);
    rectF.setY(radius*qSin(angle*M_PI/180.0) - pointSize/1.0);
    rectF.setWidth(pointSize*4);
    rectF.setHeight(pointSize*2);
    return rectF;
}
